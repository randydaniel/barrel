const config = {
  plugins: [
    require('./postcss-tasks/postcss-module-import'),
    require('postcss-inline-svg'),
    require("postcss-color-function"),
    require('precss'),
    require('autoprefixer')({
      browsers: [
        'last 3 versions',
        'iOS >= 8',
        'Safari >= 8',
        'ie 11',
      ]
    }),
    require('postcss-extend'),
    require('postcss-nested'),
  ]
}

if (process.env.ENV === 'development') {
  // Chuck in directly after importing files
  config.plugins.splice(1, 0,
    require('./postcss-tasks/postcss-shopify-fonts')('//cdn.shopify.com/s/files/1/0503/7463/9811/t/1/assets/')
  )
}

config.plugins.push(require('precss'))

module.exports = config
